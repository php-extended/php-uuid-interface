<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uuid;

use PhpExtended\Factory\FactoryInterface;

/**
 * UuidFactoryInterface interface file.
 * 
 * This factory creates uuid objects.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Factory\FactoryInterface<UuidInterface>
 */
interface UuidFactoryInterface extends FactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Factory\FactoryInterface::create()
	 */
	public function create() : UuidInterface;
	
}
