<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uuid;

use Stringable;

/**
 * UuidInterface interface file.
 * 
 * Uuids consists of unique identifiers which have a very low probability of
 * collision. Each version of uuid has its own method to be built
 * 
 * Uuids are considered immutable; all methods that might change state MUST
 * be implemented such that they retain the internal state of the current
 * uuid and return an instance that contains the changed state.
 * 
 * @author Anastaszor
 */
interface UuidInterface extends Stringable
{	
	/**
	 * The null uuid for namespacing purposes.
	 *
	 * @var string
	 */
	public const NS_NULL = '00000000-0000-0000-0000-000000000000';
	
	/**
	 * The DNS namespace. This may be used as namespace for the v3 and the v5
	 * uuid generators.
	 *
	 * @var string
	 */
	public const NS_DNS = '6ba7b810-9dad-11d1-80b4-00c04fd430c8';
	
	/**
	 * The URL namespace. This may be used as namespace for the v3 and the v5
	 * uuid generators.
	 *
	 * @var string
	 */
	public const NS_URL = '6ba7b811-9dad-11d1-80b4-00c04fd430c8';
	
	/**
	 * The OID namespace. This may be used as namespace for the v3 and the v5
	 * uuid generators.
	 *
	 * @var string
	 */
	public const NS_OID = '6ba7b812-9dad-11d1-80b4-00c04fd430c8';
	
	/**
	 * The X500 namespace. This may be used as namespace for the v3 and the v5
	 * uuid generators.
	 *
	 * @var string
	 */
	public const NS_X500 = '6ba7b814-9dad-11d1-80b4-00c04fd430c8';
	
	/**
	 * Gets the standard hexadecimal-with-dashes representation for this uuid.
	 * 
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Gets the binary representation of this uuid.
	 * 
	 * @return string
	 */
	public function toBinaryString() : string;
	
	/**
	 * Gets the hexadecimal without dashes representation of this uuid.
	 * 
	 * @return string
	 */
	public function toHexString() : string;
	
	/**
	 * Gets this uuid on integer array form.
	 * 
	 * @return array<integer, integer>
	 */
	public function toArray() : array;
	
	/**
	 * Gets the time low value of this uuid.
	 * 
	 * @return integer
	 */
	public function getTimeLow() : int;
	
	/**
	 * Gets the time mid value of this uuid.
	 * 
	 * @return integer
	 */
	public function getTimeMid() : int;
	
	/**
	 * Gets the time high value of this uuid.
	 * 
	 * @return integer
	 */
	public function getTimeHigh() : int;
	
	/**
	 * Gets the clock seq high value of this uuid (and the reserved bits).
	 * 
	 * @return integer
	 */
	public function getClockSeqHigh() : int;
	
	/**
	 * Gets the version number of this uuid.
	 *
	 * @return integer
	 */
	public function getVersion() : int;
	
	/**
	 * Gets the clock seq low value of this uuid.
	 * 
	 * @return integer
	 */
	public function getClockSeqLow() : int;
	
	/**
	 * Gets the node high value of this uuid.
	 * 
	 * @return integer
	 */
	public function getNodeHigh() : int;
	
	/**
	 * Gets the node low value of this uuid.
	 * 
	 * @return integer
	 */
	public function getNodeLow() : int;
	
	/**
	 * Gets whether this uuid equals the given uuid. An uuid equals another
	 * uuid if all of its components are equals.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $uuid
	 * @return boolean
	 */
	public function equals($uuid) : bool;
	
}
