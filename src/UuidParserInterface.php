<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uuid;

use PhpExtended\Parser\ParserInterface;

/**
 * UuidParserInterface interface file.
 * 
 * This interface is a parser to get uuid objects.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<UuidInterface>
 */
interface UuidParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
